A Simple Newsletter tool.

Features:
- optional integrated Block , 
containing a link to the Newsletter signup formular
- SignUp Page , linked to the navigation menu
- administrative interface , linked to the admin menu
- Own permission which allows Users to send Newsletters or not
- Database Table for logging sent Newsletters

No Account needed to signup for the Newsletter ,
although you have the option (Checkbox) to send the Newsletter
not only to the Users
that signed the formular but also for the registrated Users
who are signed up in your Drupal instance
to their account-email

https://www.drupal.org/sandbox/anon767/2542020

A week ago I searched for a Newsletter tool , 
that is kept simple and easy to use without much stuff around it, 
I stopped there and made my own as I
couldn't find one matching these conditions.
